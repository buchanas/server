const functions = require('firebase-functions');
const admin = require('firebase-admin');
const firebase = require('firebase');

var serviceAccount = require("./permission.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://first-firebase-project-392fe.firebaseio.com"
});

const db = admin.firestore();
const cors = require('cors');
const express = require('express');
const app = express();
app.use( cors({ origin: true}));

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const firebaseConfig = {
    apiKey: "AIzaSyAh_8DZuDRej8vCaLCLd9HEv5FIEWwY8Co",
    authDomain: "first-firebase-project-392fe.firebaseapp.com",
    databaseURL: "https://first-firebase-project-392fe.firebaseio.com",
    projectId: "first-firebase-project-392fe",
    storageBucket: "first-firebase-project-392fe.appspot.com",
    messagingSenderId: "279595208066",
    appId: "1:279595208066:web:1ea667a114df4be597539b",
    measurementId: "G-RFS2Y2484R"
  };

firebase.initializeApp(firebaseConfig);
//const auth = firebase.auth();


app.get('/api/questions/:id', async (req, res) => {
    try {
        const document = db.collection('questions').doc(req.params.id);
        let response = await document.get();
        let questions = response.data();
        res.send(questions);
    }
    catch (err) {
        res.status(500).send(err);
    }
})


app.get('/api/questions', async (req, res) => {
    try {
        const query = db.collection('questions');
        let response = [];

        await query.get().then(querySnapshot => {
            let docs = querySnapshot.docs;
            for (let doc of docs){
                const selectedItem = {
                    id: doc.id,
                    title: doc.data().title,
                    options: doc.data().options
                }
                response.push(selectedItem);
            }
            return response;
        })
            res.send(response);
    }
    catch (err) {
        res.status(500).send(err);
    }
})

app.put('/api/questions/:id', async (req, res) => {
    try {
        const document = db.collection('questions').doc(req.params.id);
        const response = await document.update({
            title: req.body.title,
            options: req.body.options
        });
        res.status(200).send('Document updated!!');
    } catch (err) {
        res.send(err);
    }

})

app.get('/api/bottles/:id', async (req, res) => {
    try {
        const document = db.collection('bottles').doc(req.params.id);
        let response = await document.get();
        let bottle = response.data();
        res.send(bottle);
    }
    catch (err) {
        res.status(500).send(err);
    }
})

app.get('/api/bottles', async (req, res) => {
    try {
        const query = db.collection('bottles');
        let bottles = [];

        await query.get().then(querySnapshot => {
            let docs = querySnapshot.docs;
            for (let doc of docs){
                const selectedItem = {
                    id: doc.id,
                    title: doc.data().title,
                    image: doc.data().image
                }
                bottles.push(selectedItem);
            }
            return bottles;
        })
            res.send(bottles);
    }
    catch (err) {
        res.status(500).send(err);
    }
})

// app.post('/api/auth', (req, res) => {
//     auth.signInWithEmailAndPassword(req.body.identifier, req.body.password)
//         .then(data => res.send(data))
//         .catch(err => res.send(err));
// })


exports.app = functions.https.onRequest(app);